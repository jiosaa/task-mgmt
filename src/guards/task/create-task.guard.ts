import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { ListsService } from "src/lists/lists.service";
import { TeamsService } from "src/teams/teams.service";

@Injectable()
export class CreateTaskGuard implements CanActivate{
    constructor(private listsService: ListsService,
        private teamsService: TeamsService){}
        TaskProjectOwnerGuard
    async canActivate(context: ExecutionContext): Promise<boolean>{
        const request = context.switchToHttp().getRequest();
        const currentUser = request.user;
        const listId = request.query.listId;
        const list = await this.listsService.findOne(listId, ['project'])
        if(list.project.user.id === currentUser.id){
            return true;
        }

        const teamId = list.project.team.id;
        const team = await this.teamsService.findOne(teamId);
  
        const isUserInTeam = team && team.users && team.users.some((teamUser) => teamUser.id === currentUser.id);
        if(isUserInTeam){
            return true;
        } 

        return false;   
    }
}