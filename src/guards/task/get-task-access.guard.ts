import { CanActivate, ExecutionContext, Injectable, NotFoundException } from "@nestjs/common";
import { Observable } from "rxjs";
import { ListsService } from "src/lists/lists.service";
import { TeamsService } from "src/teams/teams.service";


@Injectable()
export class GetTaskAccessGuard implements CanActivate{
    constructor(
        private listsService: ListsService,
        private teamsService: TeamsService
    ){}

    async canActivate(context: ExecutionContext): Promise<boolean> {
      const request = context.switchToHttp().getRequest();
      const currentUser = request.user;
      const listId = parseInt(request.params.id);
      const list = await this.listsService.findOne(listId);
      if(!list){
        throw new NotFoundException('Tasks not found for the list.')
      }
      if(list && list.project && list.project.user.id === currentUser.id){
        return true;
      }
      
      const teamId = list.project.team.id;
      const team = await this.teamsService.findOne(teamId);

      const isUserInTeam = team && team.users && team.users.some((teamUser) => teamUser.id === currentUser.id);
      if(isUserInTeam){
          return true;
      } 
      

      return false;
    }
}