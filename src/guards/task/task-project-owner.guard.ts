import { CanActivate, ExecutionContext, Injectable, NotFoundException } from "@nestjs/common";
import { TasksService } from "src/tasks/tasks.service";


@Injectable()
export class TaskProjectOwnerGuard implements CanActivate{
    constructor(private tasksService: TasksService){}

    async canActivate(context: ExecutionContext): Promise<boolean>{
        const request = context.switchToHttp().getRequest();
        const currentUser = request.user;
        const taskId = parseInt(request.params.id);

        const task = await this.tasksService.findOne(taskId);
        if(!task){
            throw new NotFoundException("Task not found.")
        }

        if(task && task.list && task.list.project && task.list.project.user.id === currentUser.id){
            return true;
        }
        return false;
    }
}