import { CanActivate, ExecutionContext, Injectable, NotFoundException } from "@nestjs/common";
import { Observable } from "rxjs";
import { ProjectsService } from "src/projects/projects.service";

@Injectable()
export class CreateListGuard implements CanActivate{
    constructor(private projectsService: ProjectsService){}

    async canActivate(context: ExecutionContext): Promise<boolean>{
      const request = context.switchToHttp().getRequest();
      const currentUser = request.user;
      const projectId = request.query.projectId;
      const project = await this.projectsService.findOne(projectId);
      if(!project){
        throw new NotFoundException('Project not found.')
      }
      if(project.user.id === currentUser.id){
        return true;
      }  
      return false;    
    }
}