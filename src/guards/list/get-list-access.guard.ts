import { CanActivate, ExecutionContext, Injectable, NotFoundException } from "@nestjs/common";
import { Observable } from "rxjs";
import { ProjectsService } from "src/projects/projects.service";
import { TeamsService } from "src/teams/teams.service";

@Injectable()
export class GetListAccessGuard implements CanActivate{
    constructor(
        private projectsService: ProjectsService, 
        private teamsService: TeamsService
    ){}

    async canActivate(context: ExecutionContext): Promise<boolean>{
        const request = context.switchToHttp().getRequest();
        const currentUser = request.user;
        const projectId = parseInt(request.params.id);
        const project = await this.projectsService.findOne(projectId); 
        if(!project){
            throw new NotFoundException('Please create a team first.')
        }
        if(project && project.user.id === currentUser.id){
            return true;
        }

        const teamId = project.team.id;
        const team = await this.teamsService.findOne(teamId);

        const isUserInTeam = team && team.users && team.users.some((teamUser) => teamUser.id === currentUser.id);
        if(isUserInTeam){
            return true;
        }
        return false;
    }
}