import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { List } from "src/lists/list.entity";
import { CurrentUser } from "src/users/decorators/user.decorator";
import { ListsService } from "src/lists/lists.service";



@Injectable()
export class ListGuard implements CanActivate {
  constructor(private listsService: ListsService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const listId = parseInt(request.params.id);

    const currentUser = request.user;

    const list = await this.listsService.findOne(listId,  ["project", "project.user"] );

    if (list && list.project && list.project.user && currentUser && list.project.user.id === currentUser.id) {
      return true; // Allow access
    }

    return false; // Deny access
  }
} 