import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { NotificationsService } from "src/notifications/notifications.service";


@Injectable()
export class NotificationGuard implements CanActivate{
    constructor(private notServices: NotificationsService){}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const notId = parseInt(request.params.id);

        const currentUser = request.user;

        const notification = await this.notServices.findOne(notId, ['recipient']);
        if (notification && notification.recipient.id === currentUser.id) {
            return true; // Allow access
        }
        return false;
    }
}