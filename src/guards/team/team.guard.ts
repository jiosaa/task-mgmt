import { CanActivate, ExecutionContext, Injectable, NotFoundException } from "@nestjs/common";
import { Observable } from "rxjs";
import { TeamsService } from "src/teams/teams.service";

@Injectable()
export class TeamGuard implements CanActivate{
    constructor(private teamsService: TeamsService){}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();

        const teamId = parseInt(request.params.id);
 
        const currentUser = request.user;

        // Get team by id
        const team = await this.teamsService.findOne(teamId, ["project", "project.user"]);
        if(!team){
            throw new NotFoundException("Team not found.")
        }
        if(team && team.project && team.project.user.id === currentUser.id){
            return true;
        }
  
        return false;
    }

}