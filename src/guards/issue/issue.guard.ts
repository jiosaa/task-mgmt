import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Observable } from "rxjs";
import { IssuesService } from "src/issues/issues.service";


@Injectable()
export class IssueGuard implements CanActivate{
    constructor(private issuesService: IssuesService){}

    async canActivate(context: ExecutionContext): Promise<boolean>{
        const request = context.switchToHttp().getRequest();

        const issueId = parseInt(request.params.id);

        const currentUser = request.user;

        const issue = await this.issuesService.findOne(issueId, ['raiser']);

        // console.log("CurrentUser:", currentUser.id);
        // console.log("Issue raiser:", issue.raiser.id)

        if(issue && issue.raiser.id === currentUser.id){
            return true;
        }
        return false;
    }
}