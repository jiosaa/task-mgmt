import { CanActivate, ExecutionContext, Injectable, NotFoundException } from "@nestjs/common";
import { TasksService } from "src/tasks/tasks.service";
import { TeamsService } from "src/teams/teams.service";
 
@Injectable()
export class GetCommentGuard implements CanActivate{
    constructor(
        private tasksService: TasksService,
        private teamsService: TeamsService
    ){}

    async canActivate(context: ExecutionContext): Promise<boolean> {
       const request = context.switchToHttp().getRequest();
       const currentUser = request.user; 
       const taskId = parseInt(request.params.id);
       const task = await this.tasksService.findOne(taskId);
       if(!task){
        throw new NotFoundException("Task not found.")
       }

       if(task && task.list.project.user.id === currentUser.id){
           return true;
       }
       const teamId = task.list.project.team.id;
       const team = await this.teamsService.findOne(teamId);
       const isUserInTeam = team && team.users && team.users.some((teamUser) => teamUser.id === currentUser.id);
       if(isUserInTeam){
           return true;
       }
       return false;
    }
}