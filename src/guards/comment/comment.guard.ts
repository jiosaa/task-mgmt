import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { Observable } from "rxjs";
import { CommentsService } from "src/comments/comments.service";


@Injectable()
export class CommentGuard implements CanActivate{
    constructor(private commentsService: CommentsService){}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const commentId = parseInt(request.params.id);

        const currentUser = request.user;

        const comment = await this.commentsService.findOne(commentId, ['creator']);
        if (comment && comment.creator.id === currentUser.id) {
            return true; // Allow access
        }
        return false;
    }
}