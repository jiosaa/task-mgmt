import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { ProjectsService } from "src/projects/projects.service";



@Injectable()
export class ProjectGuard implements CanActivate {
  constructor(private projectsService: ProjectsService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const projectId = parseInt(request.params.id);

    const currentUser = request.user;
    const project = await this.projectsService.findOne(projectId);

    if (project && currentUser && project.user.id === currentUser.id) {
      return true; // Allow access
    }
    return false; // Deny access
  }
} 