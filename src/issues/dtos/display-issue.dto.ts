import { Expose, Transform } from "class-transformer";
import { Project } from "src/projects/project.entity";

export class DisplayIssueDto{
    @Expose()
    id: number;

    @Expose()
    title: string;

    @Expose()
    description: string;

    @Expose()
    status: boolean;

    @Expose()
    image: string;

    @Transform(({obj})=>obj.project.id)
    @Expose()
    project: number;

    @Transform(({obj})=>obj.raiser.id)
    @Expose()
    raiser: number;
}