import { IsOptional, IsBoolean } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateIssueStatusDto{
    @IsOptional()
    @IsBoolean()
    @ApiProperty()
    status: boolean;
}