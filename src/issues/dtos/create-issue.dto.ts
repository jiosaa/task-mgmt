import { IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateIssueDto{
    @IsString()
    @ApiProperty()
    title: string;

    @IsString()
    @ApiProperty()
    description: string;


    @IsString()
    @ApiProperty()
    image: string;

}