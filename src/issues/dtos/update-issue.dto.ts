import { IsOptional, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateIssueDto{
    @IsOptional()
    @IsString()
    @ApiProperty()
    title: string;

    @IsOptional()
    @IsString()
    @ApiProperty()
    description: string;


    @IsOptional()
    @IsString()
    @ApiProperty()
    image: string;
}