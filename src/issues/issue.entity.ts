import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, ManyToOne } from "typeorm";
import { IsBoolean, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { Project } from "src/projects/project.entity";
import { User } from "src/users/user.entity";


@Entity()
export class Issue{
    @ApiProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty()
    @Column()
    title: string;

    @ApiProperty()
    @Column()
    description: string;

    @ApiProperty()
    @Column({ default: false})
    status: boolean;

    @ApiProperty()
    @Column()
    image: string;

    @ApiProperty()
    @ManyToOne(()=> Project, (project)=>project.issues,  {
        orphanedRowAction: 'delete',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      })
    project: Project;

    @ApiProperty()
    @ManyToOne(() => User)
    raiser: User;

}