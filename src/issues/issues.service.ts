import { Injectable, NotFoundException } from '@nestjs/common';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Issue } from './issue.entity';
import { CreateIssueDto } from './dtos/create-issue.dto';
import { Project } from 'src/projects/project.entity';
import { User } from 'src/users/user.entity';
import { CurrentUser } from 'src/users/decorators/user.decorator';
import { ProjectsService } from 'src/projects/projects.service';



@Injectable()
export class IssuesService {
    constructor(@InjectRepository(Issue)
    private repo: Repository<Issue>,
    private projectsService: ProjectsService
    ){}

    async create(createIssueDto: CreateIssueDto, project: Project, @CurrentUser() user: User){
        const existingProject = await this.projectsService.findOne(project.id);

        if(!existingProject){
            throw new NotFoundException('Project not found.')
        }

        const issue = this.repo.create(createIssueDto);
        issue.raiser = user;
        issue.project = project;

        return this.repo.save(issue);
    }

    async findIssueByProject(project: Project, options?: FindManyOptions<Issue>){
        const issues = await this.repo.find({ where: {project}, ...options });

        if(!issues || issues.length === 0){
            throw new NotFoundException('No issue found for the project.');
        }

        return issues;
    }


    findOne(id: number, relations: string[]=[]){
        if(!id){
            return null;
        }

        const options: FindOneOptions<Issue>={
            where: { id },
            relations: ['raiser', 'project', 'project.user'],
        };
        return this.repo.findOne(options);
    }


    async update(id: number, attrs:Partial<Issue>){
        const issue = await this.findOne(id);

        if(!issue){
            throw new NotFoundException("Issue not found.");
        }

        Object.assign(issue, attrs);
        return this.repo.save(issue);
    }


     // delete project
     async remove(id: number){
        const issue = await this.findOne(id);
        if (!issue){
            throw new NotFoundException('Project not found');
        }

        return this.repo.remove(issue);
    }

    
}
