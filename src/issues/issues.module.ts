import { Module } from '@nestjs/common';
import { IssuesController } from './issues.controller';
import { IssuesService } from './issues.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Issue } from './issue.entity';
import { Project } from 'src/projects/project.entity';
import { ProjectsService } from 'src/projects/projects.service';
import { Team } from 'src/teams/team.entity';
import { TeamsService } from 'src/teams/teams.service';

@Module({
  imports: [TypeOrmModule.forFeature([Issue, Project, Team])],
  controllers: [IssuesController],
  providers: [IssuesService, ProjectsService, TeamsService]
})
export class IssuesModule {}
