import { Body, Controller, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { IssuesService } from './issues.service';
import { AuthGuard } from 'src/users/guards/auth.guards';
import { CreateIssueDto } from './dtos/create-issue.dto';
import { CurrentUser } from 'src/users/decorators/user.decorator';
import { User } from 'src/users/user.entity';
import { Project } from 'src/projects/project.entity';
import { UpdateIssueDto } from './dtos/update-issue.dto';
import { UpdateIssueStatusDto } from './dtos/issue-status.dto';
import { IssueGuard } from 'src/guards/issue/issue.guard';
import { IssueStatusGuard } from 'src/guards/issue/issue-status.guard';
import { CreateIssueAndCommentGuard } from 'src/guards/create-issue-comment.guard';
import { GetIssueCommentAndTeamGuard } from 'src/guards/get-issue-comment-team.guard';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { DisplayIssueDto } from './dtos/display-issue.dto';



@ApiTags('Issues on project')
@Controller('projects')
export class IssuesController {
    constructor(private issuesService: IssuesService){}

    @Post('/issues')
    @Serialize(DisplayIssueDto)
    @ApiOperation({ summary: 'Raise issue under specific project', description: 'A current user can raise issue, iff he is owner the project or member of team.' })
    @UseGuards(AuthGuard, CreateIssueAndCommentGuard)
    @ApiBearerAuth()
    createIssue(@Body() body: CreateIssueDto,
    @Query('projectId') projectId: number, 
    @CurrentUser() user: User){

        return this.issuesService.create(body, {id:projectId} as Project, user);
    }


    @Get('/:id/issues')
    @ApiOperation({ summary: 'See issue under specific project', description: 'A current user can see issue, iff he is owner the project or member of team.' })
    @UseGuards(AuthGuard, GetIssueCommentAndTeamGuard)
    @ApiBearerAuth()
    async getIssuesByProject(@Param('id') id: string){
        const issues = await this.issuesService.findIssueByProject({id: parseInt(id)} as Project, {relations: ['raiser']});

        return issues.map(issue => {
            const dto = new DisplayIssueDto();
            dto.id = issue.id;
            dto.title = issue.title;
            dto.description = issue.description;
            dto.status = issue.status;
            dto.image = issue.image;
            dto.project = issue.project.id;
            dto.raiser = issue.raiser.id;
            return dto;

        })
    }


    @Patch('/issues/:id')
    @Serialize(DisplayIssueDto)
    @ApiOperation({ summary: 'Update issue under specific project', description: 'A current user can update issue, iff he is raiser.' })
    @UseGuards(AuthGuard, IssueGuard)
    @ApiBearerAuth()
    updateIssue(@Param('id') id: string, @Body() body: UpdateIssueDto){
        return this.issuesService.update(parseInt(id), body);
    }

    @Patch('/issues/:id/status')
    @Serialize(DisplayIssueDto)
    @ApiOperation({ summary: 'Update issue status under specific project', description: 'A current user can update issue status, iff he is project owner.' })
    @UseGuards(AuthGuard, IssueStatusGuard)
    @ApiBearerAuth()
    updateIssueStatus(@Param('id') id: string, @Body() body: UpdateIssueStatusDto){
        return this.issuesService.update(parseInt(id), body);
    }
}
  