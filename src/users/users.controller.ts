import { Controller, Post, Body, UsePipes, ValidationPipe, Get, Req, UseGuards, Patch, Param, NotFoundException } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dtos/create-user.dto';
import { AuthService } from './auth.service';
import { ApiTags, ApiBearerAuth, ApiBody, ApiOperation } from '@nestjs/swagger';
import { UserResponseInterface } from './types/user-response.interface';
import { SigninUserDto } from './dtos/signin-user.dto';
import { ExpressRequest } from 'src/types/express-request.interface';
import { CurrentUser } from './decorators/user.decorator';
import { AuthGuard } from './guards/auth.guards';
import { UpdateProfileDto } from './dtos/update-profile.dto';
import { UpdateUserDto } from './dtos/update-user.dto';
import { UserProfileDto } from './dtos/user-profile.dto';


@ApiTags('users')
@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService, 
        private readonly authService: AuthService){}

    @Post('/signup')
    @UsePipes(new ValidationPipe)
    async createUser(@Body() body: CreateUserDto): Promise<UserResponseInterface>{
        const user = await this.authService.signup(body.first_name, body.last_name, body.email, body.password); 
        // Remove password
        const userWithoutPassword = { ...user };
        delete userWithoutPassword.password;
        return this.authService.buildUserResponse(userWithoutPassword);
    }

    
    @Post('/signin')
    @UsePipes(new ValidationPipe)
    async signin(@Body() body: SigninUserDto){
        const user = await this.authService.signin(body.email, body.password);
        const userWithoutPassword = { ...user };
        delete userWithoutPassword.password;
        return this.authService.buildUserResponse(userWithoutPassword);
    }
 
    
    @Get('/user') 
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Get currently logged in user'})
    @UseGuards(AuthGuard)
    async currentUser(@Req() request: ExpressRequest,
    @CurrentUser() user:any): Promise<UserResponseInterface>{
        return this.authService.buildUserResponse(user);
    }


    @Patch('/user')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Update user profile', 
    description: "To update user info and its profile" })
    @UseGuards(AuthGuard)
    @ApiBody({
        schema: {
          properties: {
            user: { type: 'object', properties: { first_name: { type: 'string' }, last_name: { type: 'string' } } },
            profile: {
              type: 'object',
              properties: {
                phone_number: { type: 'string' },
                job_title: { type: 'string' },
                location: { type: 'string' },
                avatar: { type: 'string' },
              },
            },
          },
        },
      })
    async updateCurrentUser(@CurrentUser('id') currentUserId: number, 
    @Body('user') updateUserDto: UpdateUserDto,
    @Body('profile') updateProfileDto: UpdateProfileDto,

    ): Promise<UserProfileDto>{
        const user = await this.usersService.update(currentUserId, updateUserDto, updateProfileDto,);

        return user;
    }


    @Get('/:id')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'See user profile'})
    @UseGuards(AuthGuard)
    async getUser(@Param('id') id: string){
        const user = await this.usersService.getUserWithProfile(parseInt(id));
        if (!user){
            throw new NotFoundException('User not found!')
        }
        return user;
    }


    @Get('/search/:email')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Search user by email.'})
    @UseGuards(AuthGuard)
    async findByEmail(@Param('email') email: string){
        return this.usersService.findByEmail(email);
    }  


    // @Get('/forgot_password/:email')
    // async sendEmailForgotPassword(@Param() params):Promise<IResponse>



}
