import { UserDto } from "../dtos/user.dto";
import { User } from "../user.entity";

export interface UserResponseInterface {
    user: UserDto & { token: string};
}