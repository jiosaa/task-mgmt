import { Injectable, NotFoundException } from '@nestjs/common';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UpdateUserDto } from './dtos/update-user.dto';
import { UpdateProfileDto } from './dtos/update-profile.dto';
import { Profile } from './profile.entity';
import { UserProfileDto } from './dtos/user-profile.dto';
import { ProfileDto } from './dtos/profile.dto';
import { MailerService } from '@nestjs-modules/mailer';
import { randomBytes, scrypt as _scrypt } from "crypto";
import { promisify } from "util";

const scrypt = promisify(_scrypt);

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User) private repo: Repository<User>,
        @InjectRepository(Profile) private profileRepo: Repository<Profile>,
        private mailerService: MailerService
        ){}

    create(first_name, last_name, email, password){
        const user = this.repo.create({ first_name, last_name, email, password })

        return this.repo.save(user);
    }


    async save(user: User): Promise<User> {
        return this.repo.save(user);
    }

      
    async findOne(id: number){
        if (!id){
            return null;
        }
        const options: FindOneOptions<User> = {
            where: { id },
            relations: ['profile'],
        };

        return this.repo.findOne(options);
    }
    
    async findById(id: number):Promise<User>{
        if (!id){  
            return null;
        }
        const options: FindOneOptions<User> = {
            where: { id },
        };
        return this.repo.findOne(options);
    }

    async getUserWithProfile(id: number): Promise<UserProfileDto> {
        const options: FindOneOptions<User> = {
            where: { id },
            relations: ['profile'],
        };
        const user = await this.repo.findOne(options);
        if (!user) {
            throw new NotFoundException('User not found.')
        }

        const userDto: UserProfileDto = {
            id: user.id,
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
            profile: this.mapProfileToDto(user.profile),
        };

        return userDto;
    }

    private mapProfileToDto(profile): ProfileDto {
        const profileDto: ProfileDto = {
            id: profile.id,
            phone_number: profile.phone_number,
            job_title: profile.job_title,
            location: profile.location,
            avatar: profile.avatar,
        };

        return profileDto;
    }


    find(email: string, options?: FindManyOptions<User>){
        return this.repo.find( { where: {email}, ...options });
    }

    async findEmail(email: string): Promise<User> {
        return await this.repo.findOne({ where: { email } });
    }

    async findByEmail(email: string): Promise<UserProfileDto> {
        const options: FindOneOptions<User> = {
          where: { email },
          relations: ['profile'],
        };
        const user = await this.repo.findOne(options);
        if (!user) {
            throw new NotFoundException('User not found.')
        }
        const userDto: UserProfileDto = {
            id: user.id,
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
            profile: this.mapProfileToDto(user.profile),
        };
        return userDto;
    }
      

    async update(id: number, 
        updateUserDto: UpdateUserDto,
        updateProfileDto: UpdateProfileDto,
        ): Promise<UserProfileDto>{
        const user = await this.getUserWithProfile(id);
        if (!user){
            throw new NotFoundException('User not found.');
        }

        Object.assign(user, updateUserDto);
        Object.assign(user.profile, updateProfileDto);

        await this.profileRepo.save(user.profile);
        await this.repo.save(user);

        const updatedUser = await this.getUserWithProfile(id);
        return updatedUser;
    }


    

    // async remove(id: number){
    //     const user = await this.findOne(id);
    //     if (!user){
    //         throw new NotFoundException('User not found');
    //     }
    //     return this.repo.remove(user);
    // }



    async getUsersByIds(ids: number[]): Promise<User[]> {
        return this.repo.findByIds(ids);
      }   


    // forgot password
       
    async sendEmailForgotPassword(email: string): Promise<boolean> {
        const user = await this.findEmail(email);
      
        if (!user) {
          throw new NotFoundException('User not found.');
        }
      
        const token = await this.createForgotPasswordToken(email);
      
        if (token && token.newPasswordToken) {
          const resetLink = `http://localhost:3000/reset_password/${token.newPasswordToken}`;
          const emailSent = await this.sendResetPasswordEmail(email, resetLink);
      
          return emailSent;
        }
      
        return false;
      }



    async sendResetPasswordEmail(email: string, resetLink: string): Promise<boolean> {
        try {
            const mailOptions = {
              to: email,
              subject: 'Password Reset Request',
              text: `
              Please click the following link to reset your password: ${resetLink}`,
              
              html: `
              <p>You're receiving this email because you requested a password reset for your user account at Vintage Task Management.</p>

              <p>Please click the following link to reset your password: <a href="${resetLink}">${resetLink}</a></p>
              
              <p>Thanks for using Vintage Task Management.</p>
              `,
            };
        
            await this.mailerService.sendMail(mailOptions);
        
            return true;
          } catch (error) {

            return false;
          }
      }


    async createForgotPasswordToken(email: string): Promise<{ newPasswordToken: string }> {
        const user = await this.findEmail(email);
      
        if (!user) {
          throw new NotFoundException('User not found.');
        }
      
        // Generate a random token or use any token generation mechanism of your choice
        const newPasswordToken = generateRandomToken();
      
        // Save the token to the user's record or token store
        user.forgotPasswordToken = newPasswordToken;
        await this.save(user);
      
        return {
          newPasswordToken,
        };
      }


      async resetPassword(resetToken: string, newPassword: string): Promise<void> {
        const user = await this.repo.findOne({ where: { forgotPasswordToken: resetToken } });
        if (!user) {
          throw new NotFoundException('Invalid or expired reset token');
        }
      
        // hash user password
        //generate a salt
        const salt = randomBytes(8).toString('hex');
        // hash the salt and password together
        const hash = (await scrypt(newPassword, salt, 32)) as Buffer;
        // join the hashed and salt together
        const result = salt + '.' + hash.toString('hex');
        // Update the user's password with the new password
        user.password = result;
        user.forgotPasswordToken = null; // Clear the reset token
      
        await this.save(user);
      }
}


//generate random token for password reset
function generateRandomToken(): string {
    const tokenLength = 20;
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let token = '';
  
    for (let i = 0; i < tokenLength; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length);
      token += characters.charAt(randomIndex);
    }
  
    return token;
  }  



