import { ApiProperty } from "@nestjs/swagger";
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, OneToMany } from "typeorm";
import { Profile } from "./profile.entity";

import { Project } from "src/projects/project.entity";
import { Task } from "src/tasks/task.entity";
import { Exclude } from "class-transformer";


@Entity()
export class User {
    @ApiProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty()
    @Column()
    first_name: string;

    @ApiProperty()
    @Column()
    last_name: string;

    @ApiProperty()
    @Column()
    email: string;

    @ApiProperty()
    @Column()
    password: string;
    

    @ApiProperty()
    @Column({ default: false })
    admin: boolean;

    @OneToOne(() => Profile)
    @JoinColumn()
    profile: Profile
 
    
    @OneToMany(() => Project, (project) => project.user, { cascade: true, eager: true })
    projects: Project[]

    @OneToMany(() => Task, (task) => task.assigned_to, { cascade: true, eager: true })
    tasks: Task[]

    @ApiProperty()
    @Column({ nullable: true })
    forgotPasswordToken: string;

}


