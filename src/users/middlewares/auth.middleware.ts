import { Injectable, NestMiddleware } from "@nestjs/common";
import { NextFunction, Request, Response } from "express";
import { ExpressRequest } from "src/types/express-request.interface";
import { jwtConstants } from "../constants";
import { JwtPayload, verify } from 'jsonwebtoken';
import { UsersService } from "../users.service";


@Injectable()
export class AuthMiddleware implements NestMiddleware {
    constructor(private readonly usersService: UsersService){}

    async use(req:ExpressRequest, _:Response, next: NextFunction){
        if(!req.headers.authorization){
            req.user = null;
            next();
            return ;

        }
 
        const token = req.headers.authorization.split(' ')[1];
        
        try{
            const decode = verify(token, jwtConstants.secret) as JwtPayload;

            const user = await this.usersService.findById(decode.id);

            // console.log("USERRRR", user)
            
            req.user = user;
            next();
        }catch (err){
            req.user = null;
            next();
        }

    }
}