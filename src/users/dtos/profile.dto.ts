import { Expose } from "class-transformer";

export class ProfileDto {
    @Expose()
    id: number;

    @Expose()
    phone_number: string;

    @Expose()
    job_title: string;

    @Expose()
    location: string;

    @Expose()
    avatar: string;
}
