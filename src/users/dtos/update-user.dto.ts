import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsString, IsOptional, IsNotEmpty } from "class-validator";

export class UpdateUserDto{
    @ApiProperty()
    @IsOptional()
    @IsString()
    first_name: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    last_name: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsOptional()
    @IsEmail()
    email: string;


}