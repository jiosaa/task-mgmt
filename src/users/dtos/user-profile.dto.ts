import { Expose } from "class-transformer";
import { ProfileDto } from "./profile.dto";

export class UserProfileDto {
    @Expose()
    id: number;

    @Expose()
    first_name: string;

    @Expose()
    last_name: string;

    @Expose()
    email: string;

    @Expose()
    profile: ProfileDto;

}