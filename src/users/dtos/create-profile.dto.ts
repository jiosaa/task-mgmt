import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class CreateProfileDto{
    @ApiProperty()
    @IsString()
    phone_number: string;

    @ApiProperty()
    @IsString()
    job_title: string;

    @ApiProperty()
    @IsString()
    location: string;

    @ApiProperty()
    @IsString()
    avatar: string;

}