import { ApiProperty } from "@nestjs/swagger";
import { IsString, IsOptional } from "class-validator";

export class UpdateProfileDto{
    @IsOptional()
    @IsString()
    phone_number: string;

    @IsOptional()
    @IsString()
    job_title: string;

    @IsOptional()
    @IsString()
    location: string;

    @IsOptional()
    @IsString()
    avatar: string;

}