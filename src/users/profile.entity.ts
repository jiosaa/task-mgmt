import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from "typeorm";
import { User } from "./user.entity";

@Entity() 
export class Profile {
    @PrimaryGeneratedColumn()
    id: number
 
    @Column()
    phone_number: string;
 
    @Column()
    job_title: string

    @Column()
    location: string

    @Column()
    avatar: string;

    @OneToOne(() => User, (user) => user.profile, {onDelete: 'CASCADE'})
    user: User

    


}