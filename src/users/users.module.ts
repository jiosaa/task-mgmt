import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { AuthService } from './auth.service';
import { AuthGuard } from './guards/auth.guards';
import { ProfilesService } from './profiles.service';
import { Profile } from './profile.entity';


@Module({
  imports: [TypeOrmModule.forFeature([User, Profile]),
],
  controllers: [UsersController],
  providers: [UsersService, AuthService, AuthGuard, ProfilesService],
  exports: [UsersService]
})
export class UsersModule {}
