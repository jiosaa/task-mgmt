import { BadRequestException, Injectable, NotFoundException } from "@nestjs/common";
import { UsersService } from "./users.service";
import { randomBytes, scrypt as _scrypt } from "crypto";
import { promisify } from "util";
import { ProfilesService } from "./profiles.service";
import { Profile } from "./profile.entity";
import { User } from "./user.entity";
import { sign } from "jsonwebtoken";
import { jwtConstants } from "./constants";
import { UserResponseInterface } from "./types/user-response.interface";
import { MailerService } from "@nestjs-modules/mailer";


const scrypt = promisify(_scrypt);


@Injectable()
export class AuthService{
    constructor(private usersService: UsersService,
        private profilesService: ProfilesService,
        private mailerService: MailerService
        ){}


    async signup(first_name:string, last_name:string, email:string, password:string): Promise<User>{
        // see if email is in use
        const users = await this.usersService.find(email);
        if(users.length){
            throw new BadRequestException('Email already exists.')
        }

        // hash user password
        //generate a salt
        const salt = randomBytes(8).toString('hex');

        // hash the salt and password together
        const hash = (await scrypt(password, salt, 32)) as Buffer;

        // join the hashed and salt together
        const result = salt + '.' + hash.toString('hex');

        const user = await this.usersService.create(first_name, last_name, email, result);

        const profile =  new Profile()
        profile.phone_number = "";
        profile.job_title = "";
        profile.location = "";
        profile.avatar = "";
        profile.user = user;

        const createdProfile = await this.profilesService.create(profile);

        user.profile = createdProfile;
      

        //return a user
        return user;

    }

    generateJwt(user: User): string{
        return sign({
            id: user.id,
            email: user.email,
            first_name: user.first_name, 
            last_name: user.last_name
       }, jwtConstants.secret, 
       );


    }


    buildUserResponse(user: User): UserResponseInterface{
        const { id, first_name, last_name, email} = user;
        const token = this.generateJwt(user);
        return {           
            user: {
                id,
                first_name,
                last_name,
                email,
                token,
              },
        };
    }

    

    async signin(email: string, password: string): Promise<User>{
        
        const [user] = await this.usersService.find(email);
        if(!user){
            throw new NotFoundException('User not found!')
        }

        const [salt, storedHash] = user.password.split('.');
        const hash = (await scrypt(password, salt, 32)) as Buffer;

        if (storedHash !== hash.toString('hex')){
            throw new BadRequestException('Incorrect password!')
        }

        return user; 
    }



  }

