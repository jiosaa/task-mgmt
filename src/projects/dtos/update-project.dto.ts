import { IsString, IsOptional } from "class-validator";

export class UpdateProjectDto {
    @IsString()
    @IsOptional()
    name: string;

    @IsOptional()
    @IsString()
    description: string;

    @IsOptional()
    @IsString()
    start_date: string;

    @IsOptional()
    @IsString()
    due_date: string;

    @IsOptional()
    @IsString()
    avatar: string;
}