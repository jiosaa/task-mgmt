import { IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";


export class CreateProjectDto {
    @ApiProperty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsString()
    description: string;

    @ApiProperty()
    @IsString()
    start_date: string;

    @ApiProperty()
    @IsString()
    due_date: string;

    @ApiProperty()
    @IsString()
    avatar: string;
}