import { Expose, Transform } from "class-transformer";
import { User } from "src/users/user.entity";

export class ProjectDto {
    @Expose()
    id: number;
    @Expose()
    name: string;
    @Expose()
    description: string;
    @Expose()
    start_date: string;
    @Expose()  
    due_date: string;
    @Expose()
    avatar: string;
    
    @Transform(({ obj })=> obj.user.id)
    @Expose()
    userId: number;
}
