import { Module } from '@nestjs/common';
import { ProjectsController } from './projects.controller';
import { ProjectsService } from './projects.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from './project.entity';
import { List } from 'src/lists/list.entity';
import { Team } from 'src/teams/team.entity';
import { Issue } from 'src/issues/issue.entity';
import { TeamsService } from 'src/teams/teams.service';
import { IssuesService } from 'src/issues/issues.service';
import { Task } from 'src/tasks/task.entity';
import { TasksService } from 'src/tasks/tasks.service';
import { CommentsService } from 'src/comments/comments.service';
import { Comment } from 'src/comments/comment.entity';
import { User } from 'src/users/user.entity';
import { Profile } from 'src/users/profile.entity';
import { UsersService } from 'src/users/users.service';
import { ProfilesService } from 'src/users/profiles.service';
import { Notification } from 'src/notifications/notification.entity';
import { NotificationsService } from 'src/notifications/notifications.service';
import { ListsService } from 'src/lists/lists.service';

@Module({
  imports: [TypeOrmModule.forFeature([Project, List, Team, Issue])
],
     
  controllers: [ProjectsController],
  providers: [ProjectsService, ListsService]
})
export class ProjectsModule {}
