import { Controller, Post,
    Body, UseGuards, Get, Query, Patch, Param, Delete

} from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOperation, ApiBody } from '@nestjs/swagger';
import { ProjectsService } from './projects.service';
import { CurrentUser } from 'src/users/decorators/user.decorator';
import { User } from 'src/users/user.entity';
import { CreateProjectDto } from './dtos/create-project.dto';
import { AuthGuard } from 'src/users/guards/auth.guards';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { ProjectDto } from './dtos/project.dto';
import { UpdateProjectDto } from './dtos/update-project.dto';
import { ProjectGuard } from 'src/guards/project/project-owner.guard';


@ApiTags('Projects')
@Controller('')

export class ProjectsController {
    constructor(private projectsService: ProjectsService){}


    @Post('/projects')
    @ApiBearerAuth()
    @ApiOperation({ summary: 'Create a project', description: 'Any authenticated user can create a project.' })
    @UseGuards(AuthGuard)
    @Serialize(ProjectDto)
    createProject(@Body() body: CreateProjectDto,
    @CurrentUser() user: User){
        return this.projectsService.create(body, user);
    }



    @Get('/my-projects')
    @ApiOperation({ summary: 'Get my projects', description: 'Display projects created by current user.' })
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    getUserProjects(@CurrentUser() user: User){
        return this.projectsService.findProjectsByUser(user);
    }


    @Get('/projects')
    @ApiOperation({ summary: 'Display projects if user is in a team ', description: 'Display projects for current user if he is a member of that project team.' })
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    async getTeamProjects(@CurrentUser() user: User){
        const projects = await this.projectsService.findProjectByUserAndTeam(user);

        return projects;
    }



    @Get('/projects/name')
    @ApiOperation({ summary: 'Search projects by name', description: 'Any authenticated user can search for project.' })
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    findAllProjects(@Query('name') name: string){
        return this.projectsService.find(name);
    }

    @Get('/projects/:id')
    @ApiOperation({ summary: 'Get single project'})
    @ApiBearerAuth()
    @Serialize(ProjectDto)
    @UseGuards(AuthGuard, ProjectGuard)
    async getProject(@Param('id') id: string){
        return this.projectsService.findOne(parseInt(id));
    }


    @Patch('projects/:id')
    @ApiOperation({ summary: 'Update a project', description: 'Current user can update a project iff he is creator.' })
    @ApiBearerAuth()
    @Serialize(ProjectDto)
    @UseGuards(AuthGuard, ProjectGuard)
    updateProject(@Param('id') id: string, @Body() body: UpdateProjectDto){
        return this.projectsService.update(parseInt(id), body);
    }


    @Delete('projects/:id')
    @ApiOperation({ summary: 'Delete a project', description: 'Current user can delete a project iff he is creator.' })
    @ApiBearerAuth()
    @Serialize(ProjectDto)
    @UseGuards(AuthGuard, ProjectGuard)
    deleteProject(@Param('id') id: string){
        return this.projectsService.remove(parseInt(id));
    }
}
