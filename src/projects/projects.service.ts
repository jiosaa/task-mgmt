import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindOneOptions, FindManyOptions } from 'typeorm';
import { Project } from './project.entity';
import { CreateProjectDto } from './dtos/create-project.dto';
import { User } from 'src/users/user.entity';
import { ListsService } from 'src/lists/lists.service';
import { TeamsService } from 'src/teams/teams.service';
import { IssuesService } from 'src/issues/issues.service';

@Injectable()
export class ProjectsService {

    constructor(
        @InjectRepository(Project) 
        private repo: Repository<Project>){}

    //create project
    create(createProjectDto: CreateProjectDto, user: User){
        const project = this.repo.create(createProjectDto);
        project.user = user;
        return this.repo.save(project);
    }

    // find a single project by id
    async findOne(id: number){
        if (!id){
            return null;
        }
        const options: FindOneOptions<Project> = {
            where: { id },
            relations: ['user', 'team'],
        };
        return await this.repo.findOne(options);
    }


    // find project by name
    find(name: string, options?: FindManyOptions<Project>){
        return this.repo.find( { where: {name}, ...options });
    }

    // update project
    async update(id: number, attrs: Partial<Project>){
        const project = await this.findOne(id);
        if (!project){
            throw new NotFoundException('Project not found.');
        }
        Object.assign(project, attrs);
        return this.repo.save(project);
    }
    
    // delete project
    async remove(id: number){
        const project = await this.findOne(id);
        if (!project){
            throw new NotFoundException('Project not found');
        }

        // // Check if there are associated lists
        // if (project.lists && project.lists.length > 0) {
        //     for (const list of project.lists) {
        //         await this.listsService.remove(list.id);
        //       }
        // }

        // // Delete associated team
        // if (project.team) {
        //     await this.teamService.remove(project.team.id);
        // }


        // // Delete associated issues
        // if (project.issues && project.issues.length > 0) {
        //     for (const issue of project.issues) {
        //     await this.issuesService.remove(issue.id);
        //     }
        // }

        return this.repo.remove(project);
    }


    async findProjectsByUser(user: User, options?: FindManyOptions<Project>){
        const projects = await this.repo.find( { where: { user }, ...options });

        if (!projects || projects.length === 0){
            throw new NotFoundException('No projects found for the user.')
        }

        return projects;
    }


    async findProjectByUserAndTeam(user: User, options?: FindManyOptions<Project>){
        const projects = await this.repo
        .createQueryBuilder('project')
        .leftJoin('project.team', 'team')
        .leftJoin('team.users', 'teamUser')
        .where('teamUser.id = :userId', {userId: user.id})
        .getMany();

        if(!projects || projects.length === 0){
            throw new NotFoundException('No Projects found for the user in a team.')
        }

        return projects;
    }
}
