import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany, OneToOne, JoinColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { User } from "src/users/user.entity";
import { List } from "src/lists/list.entity";
import { Team } from "src/teams/team.entity";
import { Issue } from "src/issues/issue.entity";


@Entity()
export class Project{
    @ApiProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty()
    @Column()
    name: string;

    @ApiProperty() 
    @Column()
    description: string;

    @ApiProperty()
    @Column()
    start_date: string;

    @ApiProperty()
    @Column()
    due_date: string;
 
    @ApiProperty()
    @Column() 
    avatar: string;

    @ApiProperty()
    @ManyToOne(() => User, (user)=> user.projects)
    user: User;

    @OneToMany(() => List, (list) => list.project, {cascade: true, onDelete: 'CASCADE',})
    lists: List[]

    @OneToOne(()=> Team, (team) => team.project, {cascade: true, onDelete: 'CASCADE',})
    team: Team

    @OneToMany(() => Issue, (issue)=>issue.project, {cascade: true, onDelete: 'CASCADE',})
    issues: Issue[]
}
