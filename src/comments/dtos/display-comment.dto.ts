import { Expose, Transform } from "class-transformer";
import { Task } from "src/tasks/task.entity";

export class DisplayCommentDto{
    @Expose()
    id: number;

    @Expose()
    comment: string;

    @Expose()
    task: Task;

    @Transform(({obj})=>obj.creator.id)
    @Expose()
    creator: number;
}