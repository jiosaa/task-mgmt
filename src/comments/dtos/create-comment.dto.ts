import { IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateCommentDto{
    @IsString()
    @ApiProperty()
    comment: string;

}