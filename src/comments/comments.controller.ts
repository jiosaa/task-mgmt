import { Controller, Get, Post, Patch, Delete, Query, Body, Param, NotFoundException, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './dtos/create-comment.dto';
import { UpdateCommentDto } from './dtos/update-comments.dto';
import { AuthGuard } from 'src/users/guards/auth.guards';
import { CurrentUser } from 'src/users/decorators/user.decorator';
import { User } from 'src/users/user.entity';
import { CommentGuard } from 'src/guards/comment/comment.guard';
import { Task } from 'src/tasks/task.entity';
import { CreateCommentGuard } from 'src/guards/comment/create-comment.guard';
import { GetCommentGuard } from 'src/guards/comment/get-comment.guard';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { DisplayCommentDto } from './dtos/display-comment.dto';


@ApiTags('Comments on task')
@Controller('tasks')
@ApiBearerAuth()
export class CommentsController {

    constructor(private commentsService: CommentsService){}

    @Post('/comments')
    @Serialize(DisplayCommentDto)
    @ApiOperation({ summary: 'Create comment under specific task', description: 'A current user can create comment, iff he is owner the project or member of team only.' })
    @UseGuards(AuthGuard, CreateCommentGuard)
    createComment(@Body() body: CreateCommentDto, @Query('taskId') taskId: number, 
    @CurrentUser() user: User,){

        return this.commentsService.create(body, {id:taskId} as Task, user);
    }


    @Get('/:id/comments')
    @ApiOperation({ summary: 'See comment under specific task', description: 'A current user can see comment, iff he is owner the project or member of team only.' })
    @UseGuards(AuthGuard, GetCommentGuard)
    async getCommentByTask(@Param('id') id: string){
        const comments = await this.commentsService.findCommentByTask({ id: parseInt(id)} as Task, { relations: ['creator']});
        return comments.map(comment => {
            const dto = new DisplayCommentDto();
            dto.id = comment.id;
            dto.comment = comment.comment;
            dto.task = comment.task;
            dto.creator = comment.creator.id;
            return dto;
          });
    };


    @Patch('/comments/:id')
    @Serialize(DisplayCommentDto)
    @ApiOperation({ summary: 'Update comment under specific task', description: 'A current user can update comment, iff he is creator of the comment.' })
    @UseGuards(AuthGuard, CommentGuard)
    updateComment(@Param('id') id: string, @Body() body: UpdateCommentDto){
        return this.commentsService.update(parseInt(id), body);
    }


    @Delete('/comments/:id')
    @Serialize(DisplayCommentDto)
    @ApiOperation({ summary: 'Delete comment under specific task', description: 'A current user can delete comment, iff he is creator of the comment.' })
    @UseGuards(AuthGuard, CommentGuard)
    deleteComment(@Param('id') id: string){
        return this.commentsService.remove(parseInt(id));
    }

}
  