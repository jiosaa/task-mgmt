import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Comment } from './comment.entity';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { CreateCommentDto } from './dtos/create-comment.dto';
import { Project } from 'src/projects/project.entity';
import { ProjectsService } from 'src/projects/projects.service';
import { CurrentUser } from 'src/users/decorators/user.decorator';
import { User } from 'src/users/user.entity';
import { Task } from 'src/tasks/task.entity';
import { TasksService } from 'src/tasks/tasks.service';

@Injectable()
export class CommentsService {
    constructor(@InjectRepository(Comment)
    private repo: Repository<Comment>,
    private tasksService: TasksService){}


    async create(createCommentDto: CreateCommentDto, task: Task, @CurrentUser() user: User){
        const existingTask = await this.tasksService.findOne(task.id);
 
        if (!existingTask){
            throw new NotFoundException('Task not found.')
        }
        

        const comment = this.repo.create(createCommentDto);
        comment.creator = user;
        comment.task = task;

        return this.repo.save(comment);

    }

    async findCommentByTask(task: Task, options?: FindManyOptions<Comment>){
        const comments = await this.repo.find({ where: {task}, ...options});

        if (!comments || comments.length === 0){
            throw new NotFoundException('No comments found for the task.');
        }

        return comments;
    }

    findOne(id: number, relations: string[]=[]){
        if(!id){
            return null;
        }

        const options: FindOneOptions<Comment>={
            where: { id },
            relations: ['creator'],
        };
        return this.repo.findOne(options);
    }


    async update(id: number, attrs: Partial<Comment>){
        const comment = await this.findOne(id);

        if(!comment){
            throw new NotFoundException("Comment not found.")
        }

        Object.assign(comment, attrs);
        return this.repo.save(comment)

    }


    async remove(id: number){
        const comment = await this.findOne(id);

        if(!comment){
            throw new NotFoundException("Comment not found.")
        }
        return this.repo.remove(comment);
    }
}
 