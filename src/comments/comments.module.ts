import { Module } from '@nestjs/common';
import { CommentsController } from './comments.controller';
import { CommentsService } from './comments.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comment } from './comment.entity';
import { ProjectsService } from 'src/projects/projects.service';
import { Project } from 'src/projects/project.entity';
import { Team } from 'src/teams/team.entity';
import { TeamsService } from 'src/teams/teams.service';
import { Task } from 'src/tasks/task.entity';
import { TasksService } from 'src/tasks/tasks.service';
import { List } from 'src/lists/list.entity';
import { ListsService } from 'src/lists/lists.service';
import { User } from 'src/users/user.entity';
import { UsersService } from 'src/users/users.service';
import { Notification } from 'src/notifications/notification.entity';
import { NotificationsService } from 'src/notifications/notifications.service';
import { Profile } from 'src/users/profile.entity';
import { ProfilesService } from 'src/users/profiles.service';

@Module({
  imports: [TypeOrmModule.forFeature([Comment, Project, Team, Task, List, User, Notification, Profile])],
  controllers: [CommentsController],
  providers: [CommentsService, ProjectsService, TeamsService, TasksService, ListsService, UsersService, NotificationsService, ProfilesService]
})
export class CommentsModule {}
