import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { User } from "src/users/user.entity";
import { Task } from "src/tasks/task.entity";

@Entity()
export class Comment{
    @ApiProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty()
    @Column()
    comment: string;

    @ApiProperty()
    @ManyToOne(() => Task, (task) => task.comments,  {
        orphanedRowAction: 'delete',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      })
    task: Task;

    @ApiProperty()
    @ManyToOne(() => User)
    creator: User;
}