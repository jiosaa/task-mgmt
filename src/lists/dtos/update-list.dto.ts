import { IsString, IsOptional } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateListDto{
    @IsOptional()
    @IsString()
    @ApiProperty()
    name: string;

}