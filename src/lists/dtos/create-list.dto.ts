import { IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateListDto{
    @IsString()
    @ApiProperty()
    name: string;

}