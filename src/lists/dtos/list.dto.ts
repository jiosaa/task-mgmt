import { Expose, Transform } from "class-transformer";

export class ListDto{
    @Expose()
    id: number;

    @Expose()
    name: string;

    @Transform(({obj})=>obj.project.id)
    @Expose()
    projectId: number;
}