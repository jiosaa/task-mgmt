import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Project } from "src/projects/project.entity";
import { Task } from "src/tasks/task.entity";


@Entity()
export class List{
    @ApiProperty()
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty()
    @Column()
    name: string;


    @ApiProperty()
    @ManyToOne(() => Project, (project)=> project.lists,  {
        orphanedRowAction: 'delete',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      })
    project: Project;


    @OneToMany(() => Task, (task) => task.list, {cascade: true, onDelete: 'CASCADE',})
    tasks: Task[]
}