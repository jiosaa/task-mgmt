import { Module } from '@nestjs/common';
import { ListsController } from './lists.controller';
import { ListsService } from './lists.service';
import { ProjectsService } from 'src/projects/projects.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { List } from './list.entity';
import { Project } from 'src/projects/project.entity';
import { Team } from 'src/teams/team.entity';
import { TeamsService } from 'src/teams/teams.service';


@Module({
  imports:[TypeOrmModule.forFeature([List, Project, Team])],
  controllers: [ListsController],
  providers: [ListsService, ProjectsService, TeamsService]
})
export class ListsModule {}
