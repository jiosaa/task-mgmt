import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { List } from './list.entity';
import { FindManyOptions, Repository, FindOneOptions } from 'typeorm';
import { CreateListDto } from './dtos/create-list.dto';
import { ProjectsService } from 'src/projects/projects.service';
import { Project } from 'src/projects/project.entity';


@Injectable()
export class ListsService {
    constructor(@InjectRepository(List)
    private repo: Repository<List>,
    private projectsService: ProjectsService
    ){}

    async create(createListDto: CreateListDto, project: Project){
        const existingProject = await this.projectsService.findOne(project.id);

        if (!existingProject){
            throw new NotFoundException('Project not found.')
        }

        const list = this.repo.create(createListDto);
        list.project = project;

        return this.repo.save(list);
    }


    async findListByProject(project: Project, options?:FindManyOptions<List>){
        const lists = await this.repo.find({ where: {project }, ...options});
        if (!lists || lists.length === 0){
            throw new NotFoundException('No lists found for the project.');
        }
        return lists;
    }

 
    async findOne(id: number, relations: string[]=[]){
        if(!id){
            return null;
        }
        const options: FindOneOptions<List>={
            where: { id },
            relations: ['project', 'project.user', 'project.team'],
        };
        return await this.repo.findOne(options);
    }


    async update(id: number, attrs: Partial<List>){
        const list = await this.findOne(id);
        if(!list){
            throw new NotFoundException("List not found.")
        }

        Object.assign(list, attrs);
        return this.repo.save(list);
    }


    async remove(id: number){
        const list = await this.findOne(id);
        if(!list){
            throw new NotFoundException("List not found.")
        }
        return this.repo.remove(list);
    }


}
