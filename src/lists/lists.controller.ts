import { Controller, Get, Post, Patch, Delete, Body, Param, NotFoundException, Query, UseGuards } from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { ListsService } from './lists.service';
import { AuthGuard } from 'src/users/guards/auth.guards';
import { CreateListDto } from './dtos/create-list.dto';
import { Project } from 'src/projects/project.entity';
import { UpdateListDto } from './dtos/update-list.dto';
import { ListGuard } from 'src/guards/list/list.guard';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { ListDto } from './dtos/list.dto';
import { CreateListGuard } from 'src/guards/list/create-list.guard';
import { GetListAccessGuard } from 'src/guards/list/get-list-access.guard';


@ApiTags('Project Lists')
@ApiBearerAuth()
@Controller('projects')
export class ListsController {
    constructor(private listsService: ListsService){}
   
    @Post('/lists')
    @Serialize(ListDto)
    @ApiOperation({ summary: 'Create list under project', description: 'A current user can create list, iff he is owner the project only.' })
    @UseGuards(AuthGuard, CreateListGuard)
    createList(@Body() body: CreateListDto, @Query('projectId') projectId:number){
        return this.listsService.create(body, {id:projectId} as Project);
    }

    @Get('/:id/lists')
    @ApiOperation({ summary: 'Get lists of a project', description: 'A current user can see list, iff he is owner the project and in team of the project only.' })
    @UseGuards(AuthGuard, GetListAccessGuard)
    async getListsByProject(@Param('id') id: string){
        const lists = await this.listsService.findListByProject({ id: parseInt(id)} as Project);
        return lists;
    }


    @Patch('/lists/:id')
    @Serialize(ListDto)
    @ApiOperation({ summary: 'Update list', description: 'A current user can update list, iff he is creator of the list.' })
    @UseGuards(AuthGuard, ListGuard)
    updateList(@Param('id') id:  string, @Body() body: UpdateListDto){
        return this.listsService.update(parseInt(id), body);
    }


    @Delete('/lists/:id')
    @Serialize(ListDto)
    @ApiOperation({ summary: 'Delete list', description: 'A current user can delete list, iff he is creator of the list.' })
    @UseGuards(AuthGuard, ListGuard)
    deleteList(@Param('id') id: string){
        return this.listsService.remove(parseInt(id));
    }
}
