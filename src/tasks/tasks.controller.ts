import { Body, Controller, Param, Post, Query, Get, Patch, Delete, UseGuards } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateTaskDto } from './dtos/create-task.dto';
import { List } from 'src/lists/list.entity';
import { UpdateTaskDto } from './dtos/update-task.dto';
import { AuthGuard } from 'src/users/guards/auth.guards';
import { TaskProjectOwnerGuard } from 'src/guards/task/task-project-owner.guard';
import { UpdateTaskAccessGuard } from 'src/guards/task/update-task-access.guard';
import { GetTaskAccessGuard } from 'src/guards/task/get-task-access.guard';
import { CreateTaskGuard } from 'src/guards/task/create-task.guard';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { DisplayTaskDto } from './dtos/display-task.dto';


@ApiTags('Tasks under project list')
@Controller('projects/lists')
@ApiBearerAuth()
export class TasksController {
    constructor(private tasksService: TasksService){}
 
    @Post('/tasks')
    @UseGuards(AuthGuard, CreateTaskGuard)
    @ApiOperation({ summary: 'Create task for a project under specific list', description: 'A current user can create task, iff he is owner or in a team of the project task is created for.' })
    createTask(@Body() body: CreateTaskDto, @Query('listId') listId: number){
        return this.tasksService.create(body, {id:listId} as List);
    }TeamsService



    @Get('/tasks/:id')
    @Serialize(DisplayTaskDto)
    @UseGuards(AuthGuard, GetTaskAccessGuard)
    @ApiOperation({ summary: 'Get single task'})
    async getTask(@Param('id') id: string){
        return this.tasksService.findOne(parseInt(id));
    }


    @Get('/:id/tasks')
    @UseGuards(AuthGuard, GetTaskAccessGuard)
    @ApiOperation({ summary: 'Get tasks of a project under specific list', description: 'A current user can see tasks, iff he is owner or in a team of the project task is created for.' })
    async getTaskByList(@Param('id') id: string){
        const tasks = await this.tasksService.findTaskByList({ id: parseInt(id)} as List);
        return tasks;
    }


    @Patch('/tasks/:id')
    @ApiOperation({ summary: 'Update a task', description: 'A current user can update, iff he is owner or in a team of the project task is created for.' })
    @UseGuards(AuthGuard, UpdateTaskAccessGuard)
    updateTask(@Param('id') id:  string, @Body() body: UpdateTaskDto){
        return this.tasksService.update(parseInt(id), body);
    }


    @Patch('/tasks/:id/move')
    @ApiOperation({summary: 'Move task '})
    @UseGuards(AuthGuard, UpdateTaskAccessGuard)
    updateTaskList(@Param('id') id: string, @Body() body: UpdateTaskDto) {
        return this.tasksService.update(parseInt(id), body);
    }


    @Delete('/tasks/:id')
    @ApiOperation({ summary: 'Delete a task', description: 'A current user can delete, iff he is project owner the task is created for.' })
    @UseGuards(AuthGuard, TaskProjectOwnerGuard)
    @Serialize(DisplayTaskDto)
    deleteTask(@Param('id') id: string){
        return this.tasksService.remove(parseInt(id));
    }
}