import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';
import { FindManyOptions, Repository, FindOneOptions } from 'typeorm';
import { ListsService } from 'src/lists/lists.service';
import { List } from 'src/lists/list.entity';
import { CreateTaskDto } from './dtos/create-task.dto';
import { UpdateTaskDto } from './dtos/update-task.dto';
import { UsersService } from 'src/users/users.service';
import { TeamsService } from 'src/teams/teams.service';
import { Notification } from 'src/notifications/notification.entity';
import { NotificationsService } from 'src/notifications/notifications.service';


@Injectable()
export class TasksService {
    constructor (@InjectRepository(Task)
    private repo: Repository<Task>,
    private listsService: ListsService, 
    private usersService: UsersService, 
    private teamsService: TeamsService, 
    private notServices: NotificationsService){}


    async create(createTaskDto: CreateTaskDto, list: List){
        const existingList = await this.listsService.findOne(list.id);

        if(!existingList){
            throw new NotFoundException('Project list not found.');
        }

        const task = this.repo.create(createTaskDto);
        task.list = list;

        return this.repo.save(task);
    }

   
    async findTaskByList(list: List, options?: FindManyOptions<Task>){
        const tasks = await this.repo.find({ where: {list}, ...options});

        if (!tasks || tasks.length === 0){
            throw new NotFoundException('No Tasks found for the list.');
        }

        return tasks;
    }


    async findOne(id: number, relations: string[]=[]){
        if(!id){
            return null;
        }
        const options: FindOneOptions<Task>={
            where: { id },
            relations: ['list', 'list.project', 'list.project.team', 'assigned_to', 'list.project.user'],
        };
        const task = await this.repo.findOne(options);
        return task;
    }


    async update(id: number, attrs: UpdateTaskDto){
        const task = await this.findOne(id);
        if(!task){
            throw new NotFoundException("Task not found.")
        }

        if(attrs.list){
            const list = await this.listsService.findOne(attrs.list);
            if (!list) {
                throw new NotFoundException('List not found.');
              }

            const proj = task.list.project;
              if (!proj) {
                throw new NotFoundException('Project not found for the task.');
              }
          
            if (list.project.id !== proj.id) {
                throw new NotFoundException('List does not belong to the project.');
              }


            task.list = list;
        }

        if (attrs.assigned_to) {
            const assignedToUser = await this.usersService.findOne(attrs.assigned_to);
            if (!assignedToUser) {
              throw new NotFoundException('Assigned user not found.');
            }


            const project = task.list.project;

            if(!project){
                throw new NotFoundException('Project not found for the task.')
            }

            const team = await this.teamsService.findTeamByProject(project);
           
            if (!team) {
                throw new NotFoundException('Team not found for the task project.');
            }

            const isUserInTeam = team.users.some(user => user.id === assignedToUser.id);
    
            if (!isUserInTeam) {
              throw new NotFoundException('Assigned user is not part of the project team.');
            
            }

            const notification = new Notification();
            notification.title = 'Task Assigned';
            notification.message = `You have been assigned to a task '${task.name}' of project '${task.list.project.name}'`;
            notification.recipient= assignedToUser;
            notification.read = false;

            await this.notServices.create(notification);
      
            task.assigned_to = assignedToUser;
          }

        Object.assign(task, attrs);
        return this.repo.save(task);
    }


    async remove(id: number){
        const task = await this.findOne(id);
        if(!task){
            throw new NotFoundException("Task not found.")
        }
        return this.repo.remove(task);
    }
}
  