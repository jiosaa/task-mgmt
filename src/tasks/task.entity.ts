import { Column,
    PrimaryGeneratedColumn,
    Entity, ManyToOne, OneToMany} from "typeorm";
    import { ApiProperty } from "@nestjs/swagger";
import { List } from "src/lists/list.entity";
import { User } from "src/users/user.entity"; 
import { Comment } from "src/comments/comment.entity";
    
    @Entity()
    export class Task{
        @ApiProperty()
        @PrimaryGeneratedColumn()
        id: number;
    
        @ApiProperty()
        @Column()
        name: string;
    
        @ApiProperty()
        @Column()
        description: string;
    
        @ApiProperty()
        @Column()
        start_date: string;
    
        @ApiProperty()
        @Column()
        due_date: string;
    
        @ApiProperty()
        @ManyToOne(() => List, (list)=> list.tasks,  {
            orphanedRowAction: 'delete',
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          })
        list: List;

        @OneToMany(() => Comment, (comment) => comment.task, {cascade: true, onDelete: 'CASCADE',})
        comments: Comment[]

        @ApiProperty()
        @ManyToOne(() => User, (user)=> user.tasks)
        assigned_to: User;
    }
    