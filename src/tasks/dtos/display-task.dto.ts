import { Expose, Transform } from "class-transformer";

export class DisplayTaskDto {
    @Expose()
    id: number;
    @Expose()
    name: string;   
    @Expose()
    description: string;
    @Expose()
    start_date: string;
    @Expose()
    due_date: string;

    
    @Transform(({ obj })=> obj.list.id)
    @Expose()
    ListId: number;

}
