import { IsOptional, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateTaskDto{
    @IsString()
    @IsOptional()
    @ApiProperty()
    name: string;

    @IsString()
    @IsOptional()
    @ApiProperty()
    description: string;

 

    @IsString() 
    @IsOptional()
    @ApiProperty()
    start_date: string;


    @IsString()
    @IsOptional()
    @ApiProperty()
    due_date: string;


    @IsOptional()
    @ApiProperty()
    list: number;
  
    @IsOptional()
    @ApiProperty()
    assigned_to: number;

}