import { Module } from '@nestjs/common';
import { TasksController } from './tasks.controller';
import { TasksService } from './tasks.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Task } from './task.entity';
import { ListsService } from 'src/lists/lists.service';
import { List } from 'src/lists/list.entity';
import { Project } from 'src/projects/project.entity';
import { ProjectsService } from 'src/projects/projects.service';
import { User } from 'src/users/user.entity';
import { UsersService } from 'src/users/users.service';
import { Profile } from 'src/users/profile.entity';
import { ProfilesService } from 'src/users/profiles.service';
import { TeamsService } from 'src/teams/teams.service';
import { Team } from 'src/teams/team.entity';
import { Notification } from 'src/notifications/notification.entity';
import { NotificationsService } from 'src/notifications/notifications.service';



@Module({
  imports: [TypeOrmModule.forFeature([Task, List, Project, User, Profile, Team, Notification])],
  controllers: [TasksController],
  providers: [TasksService, ListsService, ProjectsService, UsersService, ProfilesService, TeamsService, NotificationsService]
})
export class TasksModule {}
