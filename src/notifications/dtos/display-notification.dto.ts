import { Expose, Transform } from "class-transformer";

export class DisplayNotificationDto {
    @Expose()
    id: number;
    @Expose()
    title: string;   
    @Expose()
    message: string;
    @Expose()
    read: boolean;

    @Transform(({ obj })=> obj.recipient.id)
    @Expose()
    recipientId: number;
}
