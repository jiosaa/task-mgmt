import { IsBoolean, IsOptional } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class ReadNotificationDto {
    @ApiProperty()
    @IsBoolean()
    @IsOptional()
    read: boolean;
}