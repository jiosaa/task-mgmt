import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { User } from "src/users/user.entity";

@Entity({ name: 'notifications'})
export class Notification{
    @PrimaryGeneratedColumn()
    @ApiProperty()
    id: number;

    @Column()
    @ApiProperty()
    title: string;

    @Column()
    @ApiProperty()
    message: string;

    @Column()
    @ApiProperty()
    read: boolean;

    @ManyToOne(() => User)
    @ApiProperty()
    recipient: User;

}