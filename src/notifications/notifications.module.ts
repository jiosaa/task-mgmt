import { Module } from '@nestjs/common';
import { NotificationsController } from './notifications.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Notification } from './notification.entity';
import { User } from 'src/users/user.entity';
import { UsersService } from 'src/users/users.service';
import { Task } from 'src/tasks/task.entity';
import { NotificationsService } from './notifications.service';
import { TasksService } from 'src/tasks/tasks.service';
import { List } from 'src/lists/list.entity';
import { Team } from 'src/teams/team.entity';
import { ListsService } from 'src/lists/lists.service';
import { TeamsService } from 'src/teams/teams.service';
import { Project } from 'src/projects/project.entity';
import { ProjectsService } from 'src/projects/projects.service';
import { Profile } from 'src/users/profile.entity';
import { ProfilesService } from 'src/users/profiles.service';

@Module({
  imports: [TypeOrmModule.forFeature([Notification, Task, List, User, Team, Project, Profile])],
  controllers: [NotificationsController],
  providers: [NotificationsService, TasksService, ListsService, UsersService, TeamsService, ProjectsService, ProfilesService]
})
export class NotificationsModule {}
     