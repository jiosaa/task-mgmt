import { Body, Controller, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CurrentUser } from 'src/users/decorators/user.decorator';
import { User } from 'src/users/user.entity';
import { Notification } from './notification.entity';
import { NotificationsService } from './notifications.service';
import { AuthGuard } from 'src/users/guards/auth.guards';
import { ReadNotificationDto } from './dtos/read-notification.dto';
import { NotificationGuard } from 'src/guards/notification/notification.guard';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { DisplayNotificationDto } from './dtos/display-notification.dto';

@ApiTags('Notifications')
@Controller('notifications')
export class NotificationsController {
    constructor(
        private notServices: NotificationsService
    ){}

    @Get()
    @UseGuards(AuthGuard)
    @ApiOperation({summary: 'Notification when task assigned'})
    @ApiBearerAuth()
    async getNotifications(@CurrentUser() user: User):Promise<Notification[]>{
        return await this.notServices.getNotificationsByRecipient(user.id);
    }

    @Patch('/read/:id')
    @UseGuards(AuthGuard, NotificationGuard)
    @ApiBearerAuth()
    @Serialize(DisplayNotificationDto)
    @ApiOperation({ summary: 'Mark as read.'})
    markAsRead(@Param('id') id:  string, @Body() body: ReadNotificationDto){
        return this.notServices.update(parseInt(id), body);

    }
}
