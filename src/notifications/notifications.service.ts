import { Injectable, NotFoundException } from '@nestjs/common';
import { User } from 'src/users/user.entity';
import { FindOneOptions, Repository } from 'typeorm';
import { Notification } from './notification.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class NotificationsService {
    constructor(
        @InjectRepository(Notification)
        private notRepo: Repository<Notification>,
    ){}



    create(notification: Notification){
        return this.notRepo.save(notification);
    }
    

    async getNotificationsByRecipient(recipientId: number): Promise<Notification[]>{
        return await this.notRepo.find({
            where: {recipient: {id: recipientId }},
            order: {id: 'DESC'},
        });
    }


    async findOne(id: number, relations: string[]=[]){
        if(!id){
            return null;
        }
        const options: FindOneOptions<Notification>={
            where: { id },
            relations: ['recipient'],
           
        };
        return await this.notRepo.findOne(options);
    }


    async update(id: number, attrs: Partial<Notification>){
        const notification = await this.findOne(id);
        if(!notification){
            throw new NotFoundException("Notification not found.")
        }

        Object.assign(notification, attrs);
        return await this.notRepo.save(notification);
    }
}
