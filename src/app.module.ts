import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectsModule } from './projects/projects.module';
import { TasksModule } from './tasks/tasks.module';
import { User } from './users/user.entity';
import { Project } from './projects/project.entity';
import { Task } from './tasks/task.entity';
import { Profile } from './users/profile.entity';
import { AuthMiddleware } from './users/middlewares/auth.middleware';
import { ListsModule } from './lists/lists.module';
import { TeamsModule } from './teams/teams.module';
import { CommentsModule } from './comments/comments.module';
import { IssuesModule } from './issues/issues.module';
import { List } from './lists/list.entity';
import { Team } from './teams/team.entity';
import { Comment } from './comments/comment.entity';
import { Issue } from './issues/issue.entity';
import { MailerModule } from '@nestjs-modules/mailer';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { NotificationsModule } from './notifications/notifications.module';
import { Notification } from './notifications/notification.entity';
import { AuthService } from './users/auth.service';
import { UploadsModule } from './uploads/uploads.module';




@Module({
  imports: [
    ConfigModule.forRoot(),
    // ConfigModule.forRoot({ isGlobal: true}),
    // TypeOrmModule.forRootAsync({
    //   imports: [ConfigModule],
    //   useFactory: (configService: ConfigService) => ({
    //     type: 'postgres',
    //     host: configService.get('DB_HOST'),
    //     port: +configService.get<number>('DB_PORT'),
    //     username: configService.get('DB_USERNAME'),
    //     password: configService.get('DB_PASSWORD'),
    //     database: configService.get('DB_NAME'),
    //     entities: [User],
    //     synchronize: true,
    //   }),
    //   inject: [ConfigService],
    // }),

    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'db.sqlite',
      entities: [User, Profile, Project, Task, List, Team, Comment, Issue, Notification],
      synchronize: true,
    }),

    
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        transport: {
          host: 'smtp.gmail.com',
          auth: {
            user: configService.get('MAIL_USER'),
            pass: configService.get('MAIL_PASSWORD'),
          },
        },
      }),

      inject: [ConfigService],
    }),


    UsersModule,
    ProjectsModule,
    TasksModule,
    ListsModule,
    TeamsModule,
    CommentsModule,
    IssuesModule,
    NotificationsModule,
    UploadsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule{
  configure(consumer: MiddlewareConsumer){
    consumer.apply(AuthMiddleware).forRoutes(
      {path: '*',method: RequestMethod.POST},
      {path: '*',method: RequestMethod.GET},
      {path: '*',method: RequestMethod.PATCH},
      {path: '*',method: RequestMethod.DELETE}
      )
  }
}
