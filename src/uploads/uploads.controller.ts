import { Controller, Get, Param, Post, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { diskStorage } from 'multer';
import { FileInterceptor } from '@nestjs/platform-express';
import { extname } from 'path';
import { Express, Response } from 'express';
import { ApiTags } from '@nestjs/swagger';


@ApiTags('Upload')
@Controller('uploads')
export class UploadsController {
    @Post('/upload')
    @UseInterceptors(FileInterceptor('image', {
      storage: diskStorage({
        destination: './public/images',
        filename: (req, image, callback) => {
          const uniqueSuffix = Date.now()+ '-';
          const ext = extname(image.originalname);
          const filename = `${image.originalname}-${uniqueSuffix}${ext}`;
          callback(null, filename);

        },
      }),
    }))
  
    uploadImage(@UploadedFile() image: Express.Multer.File) {
      
      return image;
    }



    @Get('/image/:filename')
    async getImage(@Param('filename') filename: string, @Res() res: Response) {
      return res.sendFile(filename, { root: './public/images' });
    }
}
