import { Project } from "src/projects/project.entity";
import { User } from "src/users/user.entity";
import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, OneToOne, JoinColumn } from "typeorm";


@Entity()
export class Team{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string; 

    @ManyToMany(() => User)
    @JoinTable()
    users: User[]



    @OneToOne(() => Project,  {
        orphanedRowAction: 'delete',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      })
    @JoinColumn()
    project: Project


}