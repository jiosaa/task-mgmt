import { Controller, Post, Query, Body, Get, Param, Patch, Delete, NotFoundException,UseGuards } from '@nestjs/common';
import { TeamsService } from './teams.service';
import { CreateTeamDto } from './dtos/create-team.dto';
import { Project } from 'src/projects/project.entity';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UsersService } from 'src/users/users.service';
import { UpdateTeamDto } from './dtos/update-team.dto';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { TeamDto } from './dtos/team.dto';
import { AuthGuard } from 'src/users/guards/auth.guards';
import { TeamGuard } from 'src/guards/team/team.guard';
import { CreateTeamGuard } from 'src/guards/team/create-team.guard';
import { GetIssueCommentAndTeamGuard } from 'src/guards/get-issue-comment-team.guard';
import { DisplayTeamDto } from './dtos/display-team.dto';

@ApiTags('Projects team')
@Controller('projects')
export class TeamsController {
    constructor(private teamsService: TeamsService, 
        private usersService: UsersService){}


    @Post('/teams')
    @ApiOperation({ summary: 'Create team for specific project', description: 'A current user can create team, iff he is owner the project.' })
    @UseGuards(AuthGuard, CreateTeamGuard)
    @Serialize(DisplayTeamDto)
    @ApiBearerAuth()
    async createTeam(
        @Body() body: CreateTeamDto, 
        @Query('projectId') projectId: number,
        @Query('membersIds') membersIds: number[],
        ){

        const project = { id: projectId } as Project;

        const members = await this.usersService.getUsersByIds(membersIds);

        return this.teamsService.create(body, project, members);
    }
  

    @Get('/:id/team')
    @ApiOperation({ summary: 'See team of specific project', description: 'A current user can see team, iff he is owner the project or member of the team.' })
    @UseGuards(AuthGuard, GetIssueCommentAndTeamGuard)
    @Serialize(DisplayTeamDto)
    @ApiBearerAuth()
    async getTeamByProject(@Param('id') id: string){
        const team = await this.teamsService.findTeamByProject({ id: parseInt(id)} as Project);
        return team;
    }



    @Patch('/teams/:id')
    @ApiOperation({ summary: 'Update team of specific project', description: 'A current user can update team, iff he is owner the project.' })
    @ApiBearerAuth()
    @UseGuards(AuthGuard, TeamGuard)
    @Serialize(DisplayTeamDto)
    async updateTeam(
        @Param('id') id:  string, @Body() body: UpdateTeamDto,
        @Query('membersIds') membersIds: number[],
    ){
        const members = await this.usersService.getUsersByIds(membersIds);

        return this.teamsService.update(parseInt(id), body, members);
    }

  
    @Delete('/teams/:id')
    @ApiOperation({ summary: 'Delete team of specific project', description: 'A current user can delete team, iff he is owner the project.' })
    @ApiBearerAuth()
    @UseGuards(AuthGuard, TeamGuard)
    @Serialize(DisplayTeamDto)
    deleteTeam(@Param('id') id: string){
        return this.teamsService.remove(parseInt(id));
    }

}
