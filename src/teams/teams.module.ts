import { Module } from '@nestjs/common';
import { TeamsController } from './teams.controller';
import { TeamsService } from './teams.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Team } from './team.entity';
import { ProjectsService } from 'src/projects/projects.service';
import { Project } from 'src/projects/project.entity';
import { User } from 'src/users/user.entity';
import { UsersService } from 'src/users/users.service';
import { Profile } from 'src/users/profile.entity';
import { ProfilesService } from 'src/users/profiles.service';

@Module({
  imports: [TypeOrmModule.forFeature([Team, Project, User, Profile])],
  controllers: [TeamsController],
  providers: [TeamsService, ProjectsService, UsersService, ProfilesService]
})
export class TeamsModule {}
