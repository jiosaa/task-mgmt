import { ApiProperty } from "@nestjs/swagger";
import { UserDto } from "src/users/dtos/user.dto";

export class TeamDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  projectId: number;

  @ApiProperty({ type: [UserDto] })
  users: UserDto[];
}
