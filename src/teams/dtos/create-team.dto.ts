import { IsString, IsNotEmpty, ArrayNotEmpty, ArrayMinSize } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateTeamDto{
    @IsNotEmpty()
    @IsString() 
    @ApiProperty()
    name: string;

    @ArrayNotEmpty() 
    @ArrayMinSize(1) 
    @ApiProperty({ type: [Number]})
    membersIds: number[]

}