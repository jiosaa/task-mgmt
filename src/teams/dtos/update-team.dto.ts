import { IsString, IsOptional, ArrayNotEmpty, ArrayMinSize } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateTeamDto{
    @IsOptional()
    @IsString()
    @ApiProperty()
    name: string;


    @IsOptional()
    @ApiProperty({ type: [Number]})
    membersIds: number[]

}