import { Expose, Transform } from "class-transformer";
import { User } from "src/users/user.entity";

export class DisplayTeamDto {
    @Expose()
    id: number;
    @Expose()
    name: string;

    
    @Transform(({ obj })=> obj.project.id)
    @Expose()
    projectId: number;

    @Expose()
    @Transform(({ obj }) => obj.users.map((user: User) => user.id))
    users: number[];
}
