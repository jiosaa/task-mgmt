import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateTeamDto } from './dtos/create-team.dto';
import { Project } from 'src/projects/project.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Team } from './team.entity';  
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { ProjectsService } from 'src/projects/projects.service';
import { User } from 'src/users/user.entity';
import { UpdateTeamDto } from './dtos/update-team.dto';

@Injectable()
export class TeamsService { 
    constructor(@InjectRepository(Team)
    private repo: Repository<Team>,
    private projectsService: ProjectsService){}


    async create(createTeamDto: CreateTeamDto, project: Project, members: User[]){

        const existingTeam = await this.repo.findOne({ where: { project } });

        if (existingTeam) {
          throw new ConflictException('A team has already been created for this project.');
        }

        const existingProject = await this.projectsService.findOne(project.id);

        if (!existingProject){
            throw new NotFoundException('Project not found.')
        }


        const team = this.repo.create(createTeamDto);

        team.project = project;
        team.users = members;

        return this.repo.save(team);
    }


    async findTeamByProject(project: Project, options?:FindOneOptions<Team>){
        const team = await this.repo.findOne({ where: {project }, relations: ['project', 'users']});
        if (!team){
            throw new NotFoundException('No team found for the project.');
        }
        return team;
    }


    async findOne(id: number, relations: string[]=[]){
        if(!id){  
            return null;
        }
        const options: FindOneOptions<Team>={
            where: { id },
            relations: ['project', 'project.user', 'users'],
        };
        return await this.repo.findOne(options);
    }


    async update(id: number, attrs: UpdateTeamDto, members: User[]){
        const team = await this.findOne(id);
        if(!team){
            throw new NotFoundException("Team not found.")
        }

        Object.assign(team, attrs);
        team.users = members;
        return this.repo.save(team);
    }


    async remove(id: number){
        const team = await this.findOne(id);
        if(!team){
            throw new NotFoundException("Team not found.")
        }
        return this.repo.remove(team);
    }



}