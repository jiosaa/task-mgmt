import { BadRequestException, Body, Controller, Get, Param, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { UsersService } from './users/users.service';
import { ApiTags } from '@nestjs/swagger';
import { ResetPasswordDto } from './users/dtos/reset-password.dto';



@ApiTags('Forgot password')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService,
    private readonly usersService: UsersService,
    ) {}


  @Get('/forgot_password/:email')
  async sendForgotPasswordEmail(@Param('email') email: string): Promise<string> {
    try {
      const isEmailSent = await this.usersService.sendEmailForgotPassword(email);
      if (isEmailSent) {
        return 'Email sent';
      } else {
        throw new BadRequestException('Email not sent');
      }
    } catch (error) {
      console.log('Error', error);
      throw new BadRequestException('Error sending email');
    }
  }



  @Post('/reset_password/:resetToken')
  async resetPassword(
    @Param('resetToken') resetToken: string,
    @Body() body: ResetPasswordDto,
  ): Promise<string>{
    try {
      await this.usersService.resetPassword(resetToken, body.password);
      return 'Password reset successfully.';
    }catch(error){
      throw new BadRequestException('Error while resetting.')
    }
  }


}




